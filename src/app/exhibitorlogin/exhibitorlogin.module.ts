import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ExhibitorloginRoutingModule } from './exhibitorlogin-routing.module';
import { ExhibitorloginComponent } from './exhibitorlogin.component';
import { ExhibitordashboardComponent } from './exhibitordashboard/exhibitordashboard.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [ExhibitorloginComponent, ExhibitordashboardComponent, ProfileComponent],
  imports: [
    CommonModule,
    ExhibitorloginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ExhibitorloginModule { }
