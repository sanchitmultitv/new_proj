import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExhibitorloginComponent } from './exhibitorlogin.component';
import { ExhibitordashboardComponent } from './exhibitordashboard/exhibitordashboard.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [{ path: '', component: ExhibitorloginComponent}, 

  {path: 'dashboard', component:ExhibitordashboardComponent},
  {path: 'profile', component:ProfileComponent}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitorloginRoutingModule { 
  
}
