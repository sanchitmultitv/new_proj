import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-exhibitorlogin',
  templateUrl: './exhibitorlogin.component.html',
  styleUrls: ['./exhibitorlogin.component.scss']
})
export class ExhibitorloginComponent implements OnInit {

  token;
  msg;
  loginExhibitor = new FormGroup({
    emails: new FormControl(''),
    pwds: new FormControl(''),
  });
  coverImage = "../../assets/img/h-about.jpg";
  videoPlay = false;
  potrait = false;

  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder:FormBuilder) { }

  ngOnInit(): void {
  }

  loggedIn() {
    let user = {
      email: this.loginExhibitor.value.emails,
      event_id: 54,
      role_id: 1,
      password : this.loginExhibitor.value.pwds,
    };
    
    console.log(user);
     // console.log("password",user.password)
    //  alert("hi")
     this.auth.loginExhibitors(user).subscribe((res => {
      localStorage.setItem('Exemail',this.loginExhibitor.value.emails );
      localStorage.setItem('Expassword',this.loginExhibitor.value.pwds );
      localStorage.setItem('exhibitor','dashboard');

      // alert("user");
      console.log(res)
      //  alert(res[0]);
      //  debugger
      console.log(res.result.exhibition_id);
      localStorage.setItem('exhibition_id',res.result.exhibition_id );
      
      console.log(res.result.video_url);

      if (res.code === 1) {
        console.log('hello',res.result)
        
        localStorage.setItem('exhibitlogin',JSON.stringify(res.result));
        this.router.navigateByUrl('exhibitorlogin/dashboard');
        if (window.innerHeight>window.innerWidth){
          this.potrait = true;
        }else{
          this.potrait = false;
        }
      } else {
        this.msg = 'Invalid Login';
        // this.videoPlay = false;
        alert('Invalid Login');
        this.loginExhibitor.reset();
      }
    }), (err: any) => {
      // this.videoPlay = false;
      console.log('error', err)
    });
    var isMobile = {
      Android: function() {
          return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function() {
          return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
      },
      any: function() {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }
      
  };
  
  
    
  }
}
