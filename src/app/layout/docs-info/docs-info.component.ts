import { Component, OnInit } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-docs-info',
  templateUrl: './docs-info.component.html',
  styleUrls: ['./docs-info.component.scss']
})
export class DocsInfoComponent implements OnInit {
docsData;
  constructor() { }

  ngOnInit(): void {
    this.docsData = JSON.parse(localStorage.getItem('exhibitData'));
    console.log('doc',this.docsData);
  }
  closePopup(){
    $('.docsModal').modal('hide');
  }
}
