import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-visitor-dashboard',
  templateUrl: './visitor-dashboard.component.html',
  styleUrls: ['./visitor-dashboard.component.scss']
})
export class VisitorDashboardComponent implements OnInit {
  salesData;
  user_profile;
  breficase_items:any=[];
  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.salesData = JSON.parse(localStorage.getItem('exhibitData'));
    this.user_profile = JSON.parse(localStorage.getItem('virtual'));
   this.getbreifcase();
  }
  getbreifcase(){
    this._fd.getBriefcaseList(this.user_profile.event_id,this.user_profile.id).subscribe((res:any)=>{
      this.breficase_items = res.result;
    });
  }
  closePopup(){
    $('.profile').modal('hide');
  }
}
