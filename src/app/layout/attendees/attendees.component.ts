import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';

@Component({
  selector: 'app-attendees',
  templateUrl: './attendees.component.html',
  styleUrls: ['./attendees.component.scss']
})
export class AttendeesComponent implements OnInit {
  attendees = [];
  names:string;
  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {
    let event_id = 123;
    // this._fd.getAttendees(event_id).subscribe((res:any)=>{
    //   this.attendees = res.result;
    // });
  }
  filterData(value){
    let event_id = 123;

    if(value.length >= 3){

      this._fd.getAttendeesbyName(event_id,value).subscribe((res:any)=>{
      this.attendees = res.result;
      console.log(this.attendees);
    });
    }
else{
  // this._fd.getAttendees(event_id).subscribe((res:any)=>{
  //   this.attendees = res.result;
  // });
}
  }
}
