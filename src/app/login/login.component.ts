import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
import {ToastrService} from 'ngx-toastr';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  token;
  msg;
  loginForm = new FormGroup({
    email: new FormControl(''),
    passwrds: new FormControl(''),
  });
  coverImage = "../../assets/img/h-about.jpg";
  videoPlay = false;
  potrait = false;
  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder:FormBuilder, private toastr:ToastrService) { }

  ngOnInit(): void {
    localStorage.setItem('user_guide', 'start');
    // this.loginForm = this.formBuilder.group({
    //   email: ['',[Validators.email,
    //    Validators.pattern("[^ @]*@[^ @]*"),
    //       emailDomainValidator]]
    // });
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }

    
  }

  loggedIn() {
    const user = {
      email: this.loginForm.value.email,
     // password: this.loginForm.get('password').value,
      event_id: 54,
      role_id: 1,
      pass: this.loginForm.value.passwrds,
    };
    console.log(user);
    var isMobile = {
      Android: function() {
          return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function() {
          return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
      },
      any: function() {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }
      
  };
  
   // console.log("password",user.password)
    this.auth.loginMethod(user).subscribe((res: any) => {
      if (res.code === 1) {
        if( isMobile.iOS() ){
          this.videoPlay = false;
          this.router.navigateByUrl('/lobby');
        }
        this.toastr.success( 'Login Succesfully!');
        this.videoPlay = true;
        localStorage.setItem('virtual', JSON.stringify(res.result));
        this.videoPlay = true;
        // let vid: any = document.getElementById('myVideo');
        // vid.play();
        this.router.navigateByUrl('/lobby');
        if (window.innerHeight>window.innerWidth){
          this.potrait = true;
        }else{
          this.potrait = false;
        }
      } else {
        this.msg = 'Invalid Login';
        this.videoPlay = false;
        this.loginForm.reset();
      }
    }, (err: any) => {
      this.videoPlay = false;
      console.log('error', err)
    });
    // this._fd.authLogin(user).subscribe(res => {
    //   if (res.code === 1) {
    //     this.videoPlay = true;
    //     localStorage.setItem('virtual', JSON.stringify(res.data));
    //     // this.router.navigate(['/lobby']);
    //     this.videoPlay = true;
    //     let vid: any = document.getElementById('myVideo');
    //     vid.play();
    //   } else {
    //     this.msg = 'Invalid Login';
    //     this.videoPlay = false;
    //     this.loginForm.reset();
    //   }
    // }, err => {
    //   this.videoPlay = false;
    //   console.log('error', err)
    // });
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }
  }
  
  endVideo() {
    // this.videoPlay = false;
    // this.potrait = false;
    this.router.navigateByUrl('/lobby');
    // let welcomeAuido:any = document.getElementById('myAudio');
    // welcomeAuido.play();
  }
}
