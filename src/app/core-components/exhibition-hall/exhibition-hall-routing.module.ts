import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExhibitionHallComponent } from './exhibition-hall.component';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';
import { AdaptComponent } from './adapt/adapt.component';
import { AriseComponent } from './arise/arise.component';
import { AchiveComponent } from './achive/achive.component';
import { chat } from "src/app/core-components/exhibition-hall/chat.js";
import { CometChat } from "@cometchat-pro/chat";
import { Exhibition2Component } from './exhibition2/exhibition2.component';
import { Exhibiton3Component } from './exhibiton3/exhibiton3.component';

const routes: Routes = [
  {path:'', component:ExhibitionHallComponent,
  children:[
    {path:'', redirectTo:'life' },
    {path:'life', component:ExhibitLifeComponent},
    {path:'lifeboy/:exhibitName', component:LifeboyComponent},
    {path:'adapt', component:AdaptComponent},
    {path:'arise', component:AriseComponent},
    {path:'achive', component:AchiveComponent},
    {path:'exhibition2', component:Exhibition2Component},
    {path:'exhibition3', component:Exhibiton3Component}

  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionHallRoutingModule { }
