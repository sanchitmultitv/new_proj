import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExhibitionHallRoutingModule } from './exhibition-hall-routing.module';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule} from 'videogular2/compiled/buffering';
import { AdaptComponent } from './adapt/adapt.component';
import { AriseComponent } from './arise/arise.component';
import { AchiveComponent } from './achive/achive.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Exhibition2Component } from './exhibition2/exhibition2.component';
import { Exhibiton3Component } from './exhibiton3/exhibiton3.component';



@NgModule({
  declarations: [ExhibitLifeComponent, LifeboyComponent, AdaptComponent, AriseComponent, AchiveComponent, Exhibition2Component, Exhibiton3Component],
  imports: [
    CommonModule,
    ExhibitionHallRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class ExhibitionHallModule { }
