import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
declare var introJs: any;
declare var $: any;
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy, AfterViewInit {
  videoEnd = false;
  receptionEnd = false;
  showVideo = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  exhibitionHall = false;
  registrationDesk = false;
  networkingLounge = false;
  intro: any;
  player: any;
  actives: any = [];
  liveMsg = false;
  videoSOurce;
  // videoUrl;
  @ViewChild('recepVideo', { static: true }) recepVideo: ElementRef;

  constructor(private router: Router, private _fd: FetchDataService, private chat: ChatService) { }
  // videoPlayer = 'https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/acma-home_1.mp4';
  ngOnInit(): void {
      this.getUserguide();
    this.stepUpAnalytics('Lobby');
    this.audiActive();
    this.chat.getconnect('toujeo-52');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'start_live') {
        this.liveMsg = true;
      }
      if (data == 'stop_live') {
        this.liveMsg = false;
      }

    }));
    let playVideo: any = document.getElementById('playVideo');
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
  }

  ngAfterViewInit() {
    this.playAudio();
  }
  closePopup() {
    $('.yvideo').modal('hide');
  }
  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      this.actives = res.result;
    })
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerWidth <= 767) {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });

    } else {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });
    }
  }
  playAudio() {
    localStorage.setItem('play', 'play');
    let abc: any = document.getElementById('myAudio');
    abc.play();
    // alert('after login audio')
  }
  playevent() {
    this.videoEnd = true;
  }
  playReception() {
    this.router.navigate(['/welcome']);
    // this.receptionEnd = true;
    // this.showVideo = true;
    // let vid: any = document.getElementById("recepVideo");
    // vid.play();

  }
  receptionEndVideo() {
    this.router.navigate(['/welcome']);

  }
  gotoAuditoriumFront() {

    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();

  }
  playAudiThreeFourEndVideo() {
    this.router.navigate(['/auditorium/front-desk']);

  }

  // gotoAuditoriumFront() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }

  playAuditoriumLeft() {
    if (this.actives[3].status == true) {
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audLeftvideo");
      vid.play();
    }
    else {
      $('.audiModal').modal('show');
    }

  }
  gotoConferenceHall() {
    this.router.navigate(['/auditorium/one']);
  }
  gotoExhibition(name) {
    if (name === 'one') {
      this.router.navigate(['/exhibitionHall']);
    } 
    if (name === 'two') {
      this.router.navigate(['/exhibitionHall/exhibition2']);
    } 
    if (name === 'three') {
      this.router.navigate(['/exhibitionHall/exhibition3']);
    }
  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/auditorium/left']);
  }
  playAuditoriumRight() {
    if (this.actives[2].status == true) {
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audRightvideo");
      vid.play();
    }
    else {
      $('.audiModal').modal('show');
    }
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/auditorium/right']);
  }
  playExhibitionHall() {
    this.exhibitionHall = true;
    this.showVideo = true;
    let vid: any = document.getElementById("exhibitvideo");
    vid.play();
  }
  gotoExhibitionHallOnVideoEnd() {
    this.router.navigate(['/exhibitionHall']);
  }
  // playRegistrationDesk() {
  //   this.registrationDesk = true;
  //   this.showVideo = true;
  //   let vid: any = document.getElementById("regDeskvideo");
  //   vid.play();
  // }
  // gotoRegistrationDeskOnVideoEnd() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }
  playNetworkingLounge() {
    this.networkingLounge = true;
    this.showVideo = true;
    let vid: any = document.getElementById("netLoungevideo");
    vid.play();
  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/networkingLounge']);
  }
  playShowVideo(src) {
    this.videoSOurce=src;
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }

  closeModalVideo() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }

  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  getUserguide() {
    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      steps: [
        {
          element: document.querySelector("#reception_pulse"),
          intro: "<div style='text-align:center'>For any queries Relating to the event please visit the Help desk.</div>"
        },
        // {
        //   element: document.querySelectorAll("#networking_pulse")[0],
        //   intro: "<div style='text-align:center'>Click a selfie, visit the wall of fame and Network at the Lounge area.</div>"
        // },
        {
          element: document.querySelectorAll("#exhibition_pulse")[0],
          intro: "<div style='text-align:center'>Do visit the stalls placed in the Exhibition halls.</div>"
        },
        {
          element: document.querySelectorAll("#exhibition_ones")[0],
          intro: "<div style='text-align:center'>You may click on exhibitor logo to visit their stall.</div>"
        },
        {
          element: document.querySelectorAll("#audLeft_pulse")[0],
          intro: "<div style='text-align:center'>Click to attend the live conference on 15th oct 11:00 am to 12:00pm.</div>"
        },
        // {
        //   element: document.querySelectorAll("#audRight_pulse")[0],
        //   intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule.</div>"
        // },
        // {
        //   element: document.querySelector("#frontaudi_pulse"),
        //   intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule.</div>"
        // },
        // {
        //   element: document.querySelectorAll("#heighlight_pulse")[0],
        //   intro: "<div style='text-align:center'>The agendas for the events running from 21st to 25th can be viewed in agenda section.</div>"
        // },
        // {
        //   element: document.querySelectorAll("#agenda_pulse")[0],
        //   intro: "<div style='text-align:center'>Please leave your valuable feedback on 25th before you leave.</div>"
        // },


      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");

    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }
  ngOnDestroy() {
    if (localStorage.getItem('user_guide') === 'start') {
      let stop = () => this.intro.exit();
      stop();
    }
    localStorage.removeItem('user_guide');
  }

  gotoExhibtion(id) {
    this.router.navigate(['/exhibitionHall/lifeboy',id]);
  }
}
