import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExhibtionTwoComponent } from './exhibtion-two/exhibtion-two.component';


const routes: Routes = [
  {path:'', component:ExhibtionTwoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionTwoRoutingModule { }
