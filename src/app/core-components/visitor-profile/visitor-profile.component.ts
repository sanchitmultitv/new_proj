import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
import * as XLSX from 'xlsx'; 

@Component({
  selector: 'app-visitor-profile',
  templateUrl: './visitor-profile.component.html',
  styleUrls: ['./visitor-profile.component.scss']
})
export class VisitorProfileComponent implements OnInit {
  salesData;
  user_profile;
  breficase_items:any=[];
  callList:any=[];
  chatHistory:any=[];
  oneChat:any=[];
  fileName= 'Visitor_al_chat.xlsx';  

  constructor(private _fd: FetchDataService) { }

  exportexcel4(): void 
      {
         /* table id is passed over here */   
         let element = document.getElementById('chat-visitor'); 
         const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);
  
         /* generate workbook and add the worksheet */
         const wb: XLSX.WorkBook = XLSX.utils.book_new();
         XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
         /* save to file */
         XLSX.writeFile(wb, this.fileName);
        
      }

  ngOnInit(): void {
    this.salesData = JSON.parse(localStorage.getItem('exhibitData'));
    this.user_profile = JSON.parse(localStorage.getItem('virtual'));
   this.getbreifcase();
   this.getScheduleCall();
   this.getChatHistory();
  }
  getbreifcase(){
    this._fd.getBriefcaseList(this.user_profile.event_id,this.user_profile.id).subscribe((res:any)=>{
      this.breficase_items = res.result;
    });
  }
  getScheduleCall(){
    this._fd.getScheduleList(this.user_profile.id).subscribe(res=>{
      console.log(res);
      this.callList = res.result;
    })
  }
  getChatone(chats){
this.oneChat = chats;
$('.chatDocsModal').modal('show');
  }
  getChatHistory(){
    this._fd.getChatData(this.user_profile.id).subscribe((res:any)=>{
      this.chatHistory = res.result;
      console.log('chatdaata', this.chatHistory);
    })
  }
  closePopup(){
    $('.profile').modal('hide');
  }

}
