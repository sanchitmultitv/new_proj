import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExhibtionThreeComponent } from './exhibtion-three/exhibtion-three.component';


const routes: Routes = [
  {path:'', component:ExhibtionThreeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionThreeRoutingModule { }
