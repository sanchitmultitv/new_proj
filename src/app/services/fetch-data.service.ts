import { Injectable } from '@angular/core';
import { Login } from './module/login';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiConstants } from './apiConfig/api.constants';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  baseUrl = environment.baseUrl;
  loginBaseUrl = environment.url;
  login = 'RecognizerApi/index.php/api/engine/authentication';


  constructor(private http: HttpClient) { }
  authLogin(user: Login): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('email', user.email);
    params = params.set('password', user.password);
    return this.http.post(`${this.loginBaseUrl}/${this.login}`, params);
  }

  getProfile(id, role_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.profile}/id/${id}/role_id/${role_id}`);
  }
  getAttendees(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}`);
  }
  getAttendeesbyName(event_id,name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}/name/${name}`);
  }
  getComments(event_id, user_id, typ): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.commentsList}/event_id/${event_id}/type/${typ}`);
  }
  postComments(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.addComment}`, comment);
  }
  getOne2oneChatList(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.chatAttendeesComments}/event_id/${event_id}/name/${name}`);
  }
  enterTochatList(receiver_id, sender_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.One2OneCommentList}/receiver_id/${receiver_id}/sender_id/${sender_id}`)
  }
  postOne2oneChat(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.one2oneCommentPost}`, comment);
  }
  getRating(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.ratingMaster}/event_id/${event_id}`);
  }
  getFeedback(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.feedbackMaster}/event_id/${event_id}`);
  }
  postFeedback(feedback: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/feedback/post`, feedback);
  }
  postRating(rating: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/rating/post`, rating);
  }
  getExhibition(title):Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.exhibition}/${title}`);
  }
  getExhibitionTwo(id):Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.exhibitiontwo}/${id}`);
  }
  
  getQuiz() {
    return this.http.get('https://opentdb.com/api.php?amount=50&category=11&type=multiple');
  }

  askQuestions(id, value,exhibit_id): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('event_id', '54');
    params = params.set('user_id', id);
    params = params.set('question', value);
    params = params.set('exhibition_id', exhibit_id);
    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestion}`, params);
  }
  askLiveQuestions(id, value, audi_id): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('user_id', id);
    params = params.set('question', value);
    params = params.set('event_id', '52');
    params = params.set('audi_id', audi_id);

    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestionLive}`, params);
  }
  getanswers(uid,exhibit_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getQuestionAnswser}/${uid}/event_id/54/exhibition_id/${exhibit_id}`)
  }

  getCard(exhibition_id){
    return this.http.get(`https://www.tbbmedialive.com/virtualapi/v1/get/card/drop/list/exhibition_id/${exhibition_id}`)
  }
  // Liveanswers(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getliveAnswser}`)
  // }
  getPollList(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  }
  getChatData(id):Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.chatHistory}/${id}`)
  }
  // getPollListTwo(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListThree(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListFour(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  postPoll(id, data, value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('poll_id', id);
    params = params.set('user_id', data);
    params = params.set('answer', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.postPolls}`, params);
  }
  getWtsappFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.wtsappAgendaFiles}`);
  }params

  getQuizList(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.quizlist}/event_id/${event_id}`);
  }
  postSubmitQuiz(quiz): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.submitQuiz}`, quiz);
  }
  getSummaryQuiz(event_id, user_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.summaryQuiz}/event_id/${event_id}/user_id/${user_id}`);
  }

  uploadCameraPic(pic): Observable<any> {
    return this.http.post(`https://tbbmedialive.com:7000/virtualapi/v1/upload/pic`, pic);
  }
  uploadsample(pic): Observable<any>{
    // return this.http.post(`https://tbbmedialive.com/Q3PCM2020/uploadblob.php`, pic);
    return this.http.post(`https://virtualapi.multitvsolution.com/upload_photo/uploadblob.php`, pic);
  }
  groupchating(): Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/serdia_1`);
  }
  groupchatingtwo(): Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/serdia_2`);
  }
  groupchatingthree(): Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/serdia_3`);
  }
  groupchatingfour(): Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/serdia_4`);
  }
  submitWheelScore(wheel:any){
    return this.http.post(`https://tbbmedialive.com:7000/virtualapi/v1/user/game/post`, wheel);
  }
  getBriefcaseList(event_id, user_id){
    return this.http.get(`${this.baseUrl}/${ApiConstants.briefcaseList}/event_id/${event_id}/user_id/${user_id}`);
  }
  postBriefcase(doc:any){
    return this.http.post(`${this.baseUrl}:7000/${ApiConstants.postBriefcase}`, doc);
  }
  postDoc(users): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    // let params = new HttpParams();
    // params = params.set('exhibition_id', users.exhibition_id);
    // params = params.set('title', users.titles);
    // params = params.set('doc', users.files);
    return this.http.post(`https://www.tbbmedialive.com/virtualapi/v1/upload/doc`,users);
  }
  postBrochure(use): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    
    return this.http.post(`https://www.tbbmedialive.com/virtualapi/v1/upload/brochure`, use);
  }

  postCard(data:any){
    return this.http.post(`${this.baseUrl}/${ApiConstants.postCardDrop}`, data);
  }
  analyticsPost(analytics: any): Observable<any>{
    return this.http.post(`https://tbbmedialive.com:7000/virtualapi/v1/analytics/user/history/add`, analytics);
  }
  activeAudi():Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.activeInactive}`);
  }
  schdeuleAcall(dates):Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.scheduleaCall}`,dates);
  }
  scheduleCallGet(locid):Observable<any> {
    return this.http.get(`https://www.tbbmedialive.com/virtualapi/v1/user/schedule/call/list/exhibition_id/${locid}`);
  }
  getScheduleList(uid):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.callListScheduled}/${uid}`);
  }
  getChatPerson(guy){
    return this.http.get(`https://www.tbbmedialive.com/virtualapi/v1/asked/question/list/exhibition_id/${guy}`);
  }

  getParticularChat(id, guy2){
    return this.http.get(`https://www.tbbmedialive.com/virtualapi/v1/asked/question/answer/get/user_id/${guy2}/event_id/54/exhibition_id/${id}/flag/desc`)
  }
  // delDoc(exhibi_id, id):Observable<any> {
  //   const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
  //   let params = new HttpParams();
  //   params = params.set('exhibition_id', exhibi_id);
  //   params = params.set('id', id);
  //   return this.http.post(`https://www.tbbmedialive.com/virtualapi/v1/delete/exhibition/doc`,params)
  // }

  postReply(data): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    // let params = new HttpParams();
    // params = params.set('exhibition_id', users.exhibition_id);
    // params = params.set('title', users.titles);
    // params = params.set('doc', users.files);
    return this.http.post(`https://www.tbbmedialive.com/virtualapi/v1/reply/question/post`,data);
  }
  getRed(): Observable<any> {
    return this.http.get(`https://www.tbbmedialive.com/virtualapi/v1/asked/question/not_replied/exhibition_id/27`);
  }
  getAnalytics(value){
    return this.http.get(`https://www.tbbmedialive.com/virtualapi/v1/analytics/user/history/event_id/54/action/${value}`);
  }
  getBrouchers(ex_id, uid): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.brouchers}/${ex_id}/user_id/${uid}`)
  }
  totalTimeSlots(ex_id,date):Observable<any> {
    return this.http.get(`https://virtualapi.multitvsolution.com/upload_photo/calender.php?exhibition_id=${ex_id}&date=${date}`) 
  }
  delCall(cancel): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('id', cancel.idd);
    params = params.set('email', cancel.emaill);
    params = params.set('time', cancel.timee);
    params = params.set('ec_name', cancel.exhii);

    return this.http.post(`https://www.tbbmedialive.com/virtualapi/v1/cancel/schedule/call`,params);
  }
  downXL(xl){
    return this.http.get(`https://virtualapi.multitvsolution.com/upload_photo/report.php?event_id=54&action=${xl}`);
  }
  // NEW PROJECT 
  delBrochure(data): Observable<any>{
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    
    return this.http.post(`${this.baseUrl}/${ApiConstants.delBroch}`,data);
  }
  Uploadvideo(data2):Observable<any>{
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    return this.http.post(`${this.baseUrl}/${ApiConstants.uploadVid}`,data2);
  }
  delDocs(data3): Observable<any>{
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    return this.http.post(`${this.baseUrl}/${ApiConstants.delDocss}`,data3);
  }
}
